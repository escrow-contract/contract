import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@openzeppelin/hardhat-upgrades";
import "./scripts/deploy";

const config: HardhatUserConfig = {
  defaultNetwork: "bsc_testnet",
  networks: {
    bsc_testnet: {
      url: "https://data-seed-prebsc-1-s1.bnbchain.org:8545",
      chainId: 97,
      gasPrice: 20000000000,
      accounts: {
        mnemonic:
          "addict cube sand dumb giant unhappy hub lazy found eight orange unaware",
      },
    },
  },
  solidity: {
    version: "0.8.24",
    settings: {
      optimizer: {
        enabled: true,
      },
    },
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts",
  },
  mocha: {
    timeout: 20000,
  },
  etherscan: {
    apiKey: "V3GAX2161S42Y8FWFFQ4PB77WAH7924X52",
  },
};

export default config;
