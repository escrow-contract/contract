// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";

contract Escrow is Initializable {
    struct User {
        uint balance;
        uint timestamp;
    }

    mapping(address => User) private users;
    uint public userCounter;
    IERC20 public token;

    event Deposit(address indexed user, uint balance, uint timestamp);
    event Withdraw(address indexed user, uint balance, uint timestamp);

    function initialize(address _token) public initializer {
        token = IERC20(_token);
    }

    function deposit(uint amount) public {
        require(amount > 0, "greater than 0");
        require(amount <= token.balanceOf(msg.sender), "insufficient");

        User storage user = users[msg.sender];
        if (user.balance == 0) userCounter = userCounter + 1;
        user.balance = user.balance + amount;
        user.timestamp = block.timestamp;

        token.transferFrom(msg.sender, address(this), amount);
        emit Deposit(msg.sender, amount, block.timestamp);
    }

    function withdraw(uint amount) public {
        User storage user = users[msg.sender];
        require(user.balance >= amount, "insufficient");

        user.balance = user.balance - amount;
        if (user.balance == 0) {
            userCounter = userCounter - 1;
        }
        token.transfer(msg.sender, amount);
        emit Withdraw(msg.sender, amount, block.timestamp);
    }

    function amountDeposited() public view returns (uint) {
        return users[msg.sender].balance;
    }

    function totalUsers() public view returns (uint) {
        return userCounter;
    }
}
