// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

abstract contract IERC20Extend is IERC20 {
    function decimals() external view virtual returns (uint);
}

contract EscrowV2 is OwnableUpgradeable {
    event Deposit(address indexed user, uint balance, uint timestamp);
    event Withdraw(address indexed user, uint balance, uint timestamp);
    event CreateUser(address indexed user, bool isMerchant);
    event Stake(address indexed user, uint amount, uint timestamp);
    event Unstake(address indexed user, uint amount, uint timestamp);

    struct User {
        uint balance;
        uint timestamp;
        bool isMerchant;
        uint staked;
    }

    mapping(address => User) private users;
    uint public userCounter;
    IERC20Extend public token;

    uint public feePercentage;

    struct Statistic {
        uint totalBalance;
        uint totalDeposited;
        uint totalStaked;
    }

    Statistic statistic;

    function initialize(address _token) public initializer {
        token = IERC20Extend(_token);
    }

    function setOwner(address _address) public {
        require(_address != address(0), "zero address");
        _transferOwnership(_address);
    }

    function setFee(uint _fee) public onlyOwner {
        feePercentage = _fee;
    }

    modifier userExist(address _address) {
        require(users[_address].timestamp != 0, "user not exist");
        _;
    }

    modifier userNotExist(address _address) {
        require(users[_address].timestamp == 0, "user existed");
        _;
    }

    function createUser(
        address _address,
        bool _isMerchant
    ) public onlyOwner userNotExist(_address) {
        users[_address] = User(0, block.timestamp, _isMerchant, 0);
        userCounter = userCounter + 1;
        emit CreateUser(_address, _isMerchant);
    }

    function getOwner() public view returns (address) {
        return owner();
    }

    function deposit(uint amount) public userExist(msg.sender) {
        require(amount > 0, "greater than 0");
        require(amount <= token.balanceOf(msg.sender), "insufficient");

        token.transferFrom(msg.sender, address(this), amount);

        statistic.totalBalance = statistic.totalBalance + amount;

        User storage user = users[msg.sender];
        user.balance = user.balance + amount;
        user.timestamp = block.timestamp;

        emit Deposit(msg.sender, amount, block.timestamp);
    }

    function withdraw(uint amount) public userExist(msg.sender) {
        require(amount > 0, "greater than 0");
        require(amount <= users[msg.sender].balance, "insufficient");

        uint fee = (amount * feePercentage) / (100 * (10 ** token.decimals()));
        uint netAmount = amount - fee;

        token.transfer(msg.sender, netAmount);
        token.transfer(owner(), fee);

        statistic.totalBalance = statistic.totalBalance - amount;

        User storage user = users[msg.sender];
        user.balance = user.balance - amount;
        user.timestamp = block.timestamp;

        emit Withdraw(msg.sender, netAmount, block.timestamp);
    }

    function stake(uint amount) public userExist(msg.sender) {
        require(amount > 0, "greater than 0");
        require(amount <= users[msg.sender].balance, "insufficient");

        User storage user = users[msg.sender];
        user.balance -= amount;
        user.staked += amount;
        user.timestamp = block.timestamp;

        statistic.totalBalance -= amount;
        statistic.totalStaked += amount;

        emit Stake(msg.sender, amount, block.timestamp);
    }

    function unstake(uint amount) public userExist(msg.sender) {
        User storage user = users[msg.sender];
        require(user.staked >= amount, "insufficient");

        user.staked -= amount;
        user.balance += amount;
        user.timestamp = block.timestamp;

        statistic.totalBalance += amount;
        statistic.totalStaked -= amount;

        emit Unstake(msg.sender, amount, block.timestamp);
    }

    function getTotalBalance() public view returns (uint) {
        return statistic.totalBalance;
    }

    function getTotalStaked() public view returns (uint) {
        return statistic.totalStaked;
    }

    function getUser(address _address) public view returns (User memory) {
        return users[_address];
    }
}
