import { Contract, JsonRpcProvider, parseUnits, Wallet } from "ethers";
import { task } from "hardhat/config";
import { EscrowV2__factory } from "../typechain-types";
import axios from "axios";
import { TransactionResponse } from "./trx-response.interface";

task("deploy-token").setAction(async (args, hre) => {
  const factory = await hre.ethers.getContractFactory("Token");
  const token = await hre.upgrades.deployProxy(factory, ["VTB", "VTB"], {
    initializer: "initialize",
  });
  await token.waitForDeployment();
  console.log("Token deployed to:", await token.getAddress());
  // 0x89374703d460608d749B78429b34C8134BD844ff
});

task("deploy-contract")
  .addParam("contract")
  .setAction(async (args: { contract: string }, hre) => {
    const factory = await hre.ethers.getContractFactory("Escrow");
    const contract = await hre.upgrades.deployProxy(factory, [args.contract], {
      initializer: "initialize",
    });
    await contract.waitForDeployment();
    console.log("Contract deployed to:", await contract.getAddress());
    // 0x555FA26510d607fF48ed69f082a30535446B92C5
  });

task("validate-contract")
  .addParam("address")
  .setAction(async ({ address }, { ethers, upgrades }) => {
    const factory = await ethers.getContractFactory("EscrowV2");
    await upgrades.validateUpgrade(address, factory);
  });

task("deploy-contract-v2")
  .addParam("address")
  .setAction(async ({ address }, hre) => {
    const wallet = new Wallet(
      "5e126f6be27a7e2ee87cf99b19cf3883a536519495876686716cf3a590d189f3",
      new JsonRpcProvider("https://data-seed-prebsc-1-s1.bnbchain.org:8545")
    );
    const factory = await hre.ethers.getContractFactory("EscrowV2");
    const contract = await hre.upgrades.upgradeProxy(address, factory);

    const walletAddress = await wallet.getAddress();
    console.log(walletAddress);

    const data = factory.interface.encodeFunctionData("setOwner", [
      walletAddress,
    ]);
    const txRes = await wallet.sendTransaction({
      to: address,
      data,
    });
    await txRes.wait();
    const data2 = factory.interface.encodeFunctionData("setFee", [
      parseUnits("0.1", 18),
    ]);

    const txRes2 = await wallet.sendTransaction({
      to: address,
      data: data2,
    });
    await txRes2.wait();

    await contract.waitForDeployment();
    console.log("Escrow deployed to:", await contract.getAddress());
  });

const config = task("get-tx")
  .addParam("address")
  .addParam("apiKey")
  .setAction(
    async ({ address, apiKey }: { address: string; apiKey: string }) => {
      const bscTestnetApi = "https://api-testnet.bscscan.com/api";
      const res = await axios.get<TransactionResponse>(bscTestnetApi, {
        params: {
          module: "account",
          action: "txlist",
          address: address, // 0x89374703d460608d749B78429b34C8134BD844ff
          startblock: 0,
          endblock: 99999999,
          page: 1,
          offset: 10,
          sort: "asc",
          apikey: apiKey, // V3GAX2161S42Y8FWFFQ4PB77WAH7924X52
        },
      });
      const txs = res.data.result;
      const txWithTransfer = txs.filter((t) =>
        t.functionName.startsWith("transfer")
      );
      console.log(txWithTransfer);
    }
  );
