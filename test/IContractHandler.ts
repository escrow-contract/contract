import { DeployProxyOptions } from "@openzeppelin/hardhat-upgrades/dist/utils";
import {
  BaseContract,
  ContractFactory,
  FunctionFragment,
  TransactionReceipt,
  Wallet,
} from "ethers";
import { ethers, upgrades } from "hardhat";

interface ContractRes<T> {
  contract: T;
  contractAddress: string;
  txReceipts?: (TransactionReceipt | null)[];
}

interface Options {
  sendTransactions?: {
    txs: () => Tx[];
    wallet: Wallet;
  };
}

interface Tx {
  fragment: string | FunctionFragment;
  values?: readonly any[] | undefined;
}

export class IContractFactory {
  static deployContract = async <T extends BaseContract>(
    contractName: string,
    args?: unknown[],
    opts?: DeployProxyOptions
  ): Promise<ContractRes<T>> => {
    const factory = await ethers.getContractFactory(contractName);
    console.log(`Deploying...`);
    const contract = (await upgrades.deployProxy(
      factory,
      args,
      opts
    )) as unknown as T;

    await contract.waitForDeployment();
    const contractAddress = await contract.getAddress();
    console.log(`${contractName} deployed to ${contractAddress}`);
    console.log("--------------------------------------------------");
    return { contract, contractAddress };
  };

  static validateContract = async (
    contractAddress: string,
    contractName: string
  ) => {
    console.log("Validating...");
    await upgrades.validateUpgrade(
      contractAddress,
      await ethers.getContractFactory(contractName)
    );
    console.log("Validate successful!!!");
    console.log("--------------------------------------------------");
  };

  static upgradeContract = async <T extends BaseContract>(
    contractAddress: string,
    contractName: string,
    options?: Options
  ): Promise<ContractRes<T>> => {
    const factory = await ethers.getContractFactory(contractName);
    console.log("Upgrading...");
    const contract = await upgrades.upgradeProxy(contractAddress, factory);
    let txReceipts: (TransactionReceipt | null)[] = [];
    if (options?.sendTransactions) {
      const txs = options.sendTransactions.txs();
      const wallet = options.sendTransactions.wallet;
      txReceipts = await IContractFactory.sendTransactions(
        contractAddress,
        wallet,
        factory,
        txs
      );
    }
    await contract.waitForDeployment();
    console.log("Upgraded!!!");
    console.log("--------------------------------------------------");
    return {
      contract: contract as unknown as T,
      contractAddress,
      txReceipts: txReceipts,
    };
  };

  private static sendTransactions = async (
    contractAddress: string,
    wallet: Wallet,
    factory: ContractFactory<any[], BaseContract>,
    txs: Tx[]
  ) => {
    console.log("--- Send transactions ---");
    let txReceipts: (TransactionReceipt | null)[] = [];
    for (let index = 0; index < txs.length; index++) {
      const tx = txs[index];
      console.log("Send transaction: ", tx.fragment);
      const data = factory.interface.encodeFunctionData(tx.fragment, tx.values);
      const txRes = await wallet.sendTransaction({
        to: contractAddress,
        data: data,
      });
      txReceipts.push(await txRes.wait());
    }
    return txReceipts;
  };
}
