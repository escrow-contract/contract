import { ethers } from "hardhat";
import { HardhatEthersSigner } from "@nomicfoundation/hardhat-ethers/signers";
import { Escrow, EscrowV2, Token } from "../typechain-types";
import { IContractFactory as IContracHandler } from "./IContractHandler";
import { Wallet, parseUnits } from "ethers";
import { expect } from "chai";

describe("Escrow", function () {
  let token: Token;
  let tokenAddress: string;
  let escrow: Escrow;
  let escrowAddress: string;
  let escrowV2: EscrowV2;

  let owner: HardhatEthersSigner;
  let user1: HardhatEthersSigner;
  let user2: HardhatEthersSigner;

  const deployToken = async () => {
    const res = await IContracHandler.deployContract<Token>("Token", [
      "KCE",
      "KCE",
    ]);
    token = res.contract;
    tokenAddress = res.contractAddress;
  };

  const deployEscrow = async () => {
    const res = await IContracHandler.deployContract<Escrow>("Escrow", [
      tokenAddress,
    ]);
    escrow = res.contract;
    escrowAddress = res.contractAddress;
    await IContracHandler.validateContract(escrowAddress, "EscrowV2");
    const res2 = await IContracHandler.upgradeContract<EscrowV2>(
      escrowAddress,
      "EscrowV2",
      {
        sendTransactions: {
          wallet: new Wallet(
            "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80",
            new ethers.JsonRpcProvider("http://127.0.0.1:8545")
          ),
          txs: () => {
            return [
              {
                fragment: "setOwner",
                values: [owner.address],
              },
              {
                fragment: "setFee",
                values: [ethers.parseUnits("0.1", 18)],
              },
            ];
          },
        },
      }
    );
    escrowV2 = res2.contract;
  };

  before("should deploy contracts", async () => {
    [owner, user1, user2] = await ethers.getSigners();
    await deployToken();
    await deployEscrow();

    expect(await escrowV2.getOwner()).equal(owner.address);

    token.mint(owner, ethers.parseUnits("1000", 18));
    token.mint(user1, ethers.parseUnits("1000", 18));
    token.mint(user2, ethers.parseUnits("1000", 18));

    await token.connect(owner).approve(escrowAddress, ethers.MaxUint256);
    await token.connect(user1).approve(escrowAddress, ethers.MaxUint256);
    await token.connect(user2).approve(escrowAddress, ethers.MaxUint256);
  });

  it("should create new user", async () => {
    await escrowV2.connect(owner).createUser(user1.address, true);

    await expect(
      await escrowV2.connect(user1).deposit(ethers.parseUnits("100", 18))
    ).to.emit(escrow, "Deposit");

    expect(await token.balanceOf(user1)).equal(
      parseUnits("1000", 18) - parseUnits("100", 18)
    );

    const user = await escrowV2.getUser(user1.address);
    expect(user[0]).equal(parseUnits("100", 18));
  });

  it("should withdraw", async () => {
    const amount = ethers.parseUnits("100", 18);
    await expect(await escrowV2.connect(user1).withdraw(amount)).to.emit(
      escrowV2,
      "Withdraw"
    );

    console.log(await token.balanceOf(owner));
    console.log(await token.balanceOf(user1));
  });

  // it("should allow a user to withdraw tokens", async () => {
  //   const amount = ethers.parseUnits("50", 18);
  //   await expect(await escrow.connect(user1).withdraw(amount)).to.emit(
  //     escrow,
  //     "Withdraw"
  //   );
  //   const remainingAmount = await escrow.connect(user1).amountDeposited();
  //   expect(remainingAmount).to.equal(amount); // 100 - 50 = 50
  // });

  // it("should correctly update user count", async () => {
  //   const amount = ethers.parseUnits("100", 18);

  //   await token.connect(user2).approve(escrowAddress, amount);
  //   await escrow.connect(user2).deposit(amount);

  //   const userCount = await escrow.totalUsers();
  //   expect(userCount).to.equal(2);

  //   await escrow.connect(user1).withdraw(ethers.parseUnits("50", 18));
  //   const newUserCount = await escrow.totalUsers();
  //   expect(newUserCount).to.equal(1);
  // });
});
